import { Component, Input, Type, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

export interface DialogData {
  mode: 'add' | 'edit';
  item: any;
}

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
})
export class DataTableComponent {
  readonly rootUrl = 'https://localhost:7001';
  private _apiRoute: string = '';
  data: Array<any> = [];
  autoDataColumns: Array<string> = [];
  @Input() title: string = 'Table';
  @Input() formDialogType!: Type<unknown>;
  @Input() moreDialogType!: Type<unknown>;

  @ViewChild(MatTable) private _table: MatTable<any> | undefined;

  get displayedColumns() {
    return this.autoDataColumns.length > 0
      ? [...this.autoDataColumns, 'controls']
      : [];
  }

  constructor(public dialog: MatDialog, private http: HttpClient) {}

  @Input()
  set apiRoute(apiRoute: string) {
    this._apiRoute = apiRoute;
    this.updateData();
  }

  updateData() {
    this.http.get<Object[]>(this.rootUrl + this._apiRoute).subscribe({
      next: (arr) => {
        if (arr?.length < 0) return;
        this.data = [...arr];

        this.autoDataColumns = Object.keys(arr[0]).filter(
          (key) => key !== 'id'
        );

        for (const row of this.data) {
          for (const cell of Object.keys(row)) {
            if (row[cell] instanceof Object)
              row[cell] = row[cell][Object.keys(row[cell])[0]];
          }
        }

        this._table?.renderRows();
      },
    });
  }

  deleteClickHandler(item: any) {
    this.http.delete(this.rootUrl + this._apiRoute + `/${item.id}`).subscribe({
      next: () => {
        this.updateData();
      },
    });
  }

  addClickHandler() {
    this.dialog.open(this.formDialogType, { data: { mode: 'add', item: {} } });
    this.updateData();
  }

  editClickHandler(item: any) {
    this.dialog.open(this.formDialogType, { data: { mode: 'edit', item } });
    this.updateData();
  }

  moreClickHandler(item: any) {
    this.dialog.open(this.moreDialogType, { data: { item } });
    this.updateData();
  }
}
