import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../data-table/data-table.component';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-treatment-form-dialog',
  templateUrl: './treatment-form-dialog.component.html',
  styleUrls: ['./treatment-form-dialog.component.scss'],
})
export class TreatmentFormDialogComponent {
  readonly apiRoute: string = 'https://localhost:7001/treatments';

  form: FormGroup = new FormGroup({
    id: new FormControl(this.data.item.id ?? 0),
    time: new FormControl(this.data.item.time),
    patient: new FormControl(this.data.item.patient),
    description: new FormControl(this.data.item.description),
  });

  constructor(
    public dialogReg: MatDialogRef<TreatmentFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient
  ) {}

  add() {
    this.http.post(this.apiRoute, this.form.getRawValue()).subscribe({
      next: (response) => {
        console.log(response);
      },
    });
  }

  edit() {
    this.http
      .put(this.apiRoute + `/${this.data.item.id}`, this.form.getRawValue())
      .subscribe({
        next: (response) => {
          console.log(response);
        },
      });
  }
}
