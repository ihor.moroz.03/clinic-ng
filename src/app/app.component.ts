import { Component } from '@angular/core';
import { PatientFormDialogComponent } from './form-dialogs/patient-form-dialog/patient-form-dialog.component';
import { PatientTreatmentsDialogComponent } from './patient-treatments-dialog/patient-treatments-dialog.component';
import { TreatmentFormDialogComponent } from './form-dialogs/treatment-form-dialog/treatment-form-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'clinic';
  PatientFormDialogType = PatientFormDialogComponent;
  PatientTreatmentsDialogType = PatientTreatmentsDialogComponent;
  TreatmentFormDialogType = TreatmentFormDialogComponent;
}
