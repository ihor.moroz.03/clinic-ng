import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientTreatmentsDialogComponent } from './patient-treatments-dialog.component';

describe('PatientTreatmentsDialogComponent', () => {
  let component: PatientTreatmentsDialogComponent;
  let fixture: ComponentFixture<PatientTreatmentsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientTreatmentsDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PatientTreatmentsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
