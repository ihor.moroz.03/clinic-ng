import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../data-table/data-table.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-patient-form-dialog',
  templateUrl: './patient-treatments-dialog.component.html',
  styleUrls: ['./patient-treatments-dialog.component.scss'],
})
export class PatientTreatmentsDialogComponent {
  apiRoute: string;
  treatments: Observable<Object[]>;

  constructor(
    public dialogReg: MatDialogRef<PatientTreatmentsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private http: HttpClient
  ) {
    this.apiRoute = `https://localhost:7001/patients/${this.data.item.id}/treatments`;
    console.log(this.apiRoute);
    this.treatments = this.http.get<Object[]>(this.apiRoute);
    // .subscribe({
    //   next: (arr) => {
    //     if (arr?.length < 0) return;
    //     this.treatments = [...arr];
    //   },
    // });
  }
}
