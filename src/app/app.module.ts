import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';

import { AppComponent } from './app.component';
import { DataTableComponent } from './data-table/data-table.component';
import { PatientFormDialogComponent } from './form-dialogs/patient-form-dialog/patient-form-dialog.component';
import { PatientTreatmentsDialogComponent } from './patient-treatments-dialog/patient-treatments-dialog.component';
import { TreatmentFormDialogComponent } from './form-dialogs/treatment-form-dialog/treatment-form-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    DataTableComponent,
    PatientFormDialogComponent,
    PatientTreatmentsDialogComponent,
    TreatmentFormDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatTableModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
